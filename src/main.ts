/**### DOM ### */
const $notCount = document.getElementById("count-notification") as HTMLSpanElement,
	$readAll = document.getElementById("mark-all-read") as HTMLParagraphElement,
	$dot = document.querySelectorAll(".circle-red"),
	$notification = document.querySelectorAll(".main-info .rounded-xl");
let count = +$notCount.textContent!;

export function clickNot(data: NodeListOf<Element>) {
	data.forEach((item, index) => {
		item.addEventListener("click", () => {
			if (item.classList.contains("active")) {
				item.classList.remove("active");
				$dot.item(index)!.classList.add("invisible");
				count--;
				$notCount.textContent = `${count}`;
			}
		});
	});
}

$readAll.addEventListener("click", () => {
	$notification.forEach((item, index) => {
		if (item.classList.contains("active")) {
			item.classList.remove("active");
			$dot.item(index)!.classList.add("invisible");
		}
	});
	$notCount.textContent = "0";
});
window.onload = () => {
	clickNot($notification);
};
