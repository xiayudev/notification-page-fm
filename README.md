# Frontend Mentor - Interactive card details form solution

This is a solution to the [Interactive card details form challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-card-details-form-XpS8cKZDWw). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - Interactive card details form solution](#frontend-mentor---interactive-card-details-form-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- Distinguish between "unread" and "read" notifications
- Select "Mark all as read" to toggle the visual state of the unread notifications and set the number of unread messages to zero
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page

### Screenshot

- Mobile design ![Mobile](/public/images/mobile-design.png)
- Desktop design ![Desktop](/public/images/desktop-design.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/xiayudev/notification-page-fm)
- Live Site URL: [Netlify](https://notificaction-page-fmentor-thesunland.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- [Tailwind CSS](https://tailwindcss.com/) - CSS library
- [Vite](https://vite.dev)
- [Typescript](https://typescriptlang.org/) - For Logic

### What I learned
I've learned how to manipulate DOM and how to add events.
### Continued development
I am currently learning Typescript, so I want to master it.

## Author

- Website - [xiayudev](https://my-portfolio-v2-1.pages.dev/)
- Frontend Mentor - [@TheSunLand7](https://www.frontendmentor.io/profile/TheSunLand7)
- Twitter - [@J7Jeo](https://www.twitter.com/J7Jeo)
